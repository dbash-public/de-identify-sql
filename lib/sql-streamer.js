const stream = require('stream');
const SQLWriter = require('./sql-writer');
const { cleanName, buildSchema } = require('./tools');

const { SQLBuffer, BUFFERS: {
  BRACE_OPEN_BUFFER,
  COMMAND_EXEC_BUFFER,
  CREATE_TABLE_BUFFER,
  INSERT_INTO_BUFFER,
  USE_BUFFER,
  VALUES_BUFFER,
} } = require('./sql-buffer');

const STATES = {
  INSERT: 0,
  USE: 1,
  CREATE: 2,
  SEEKING_EOL: 3,
};

class SQLStreamer extends stream.Writable {
  constructor(config = {
    functionsFile: null,
    outputFile: null,
    strategyDirectory: null,
  }) {
    super();

    Object.assign(this, config);

    this.buffer = new SQLBuffer();
    this.sqlWriter = new SQLWriter(config);
    this.allText = '';
    this.databases = { default: { tables: {} } };
    this.currentDatabaseName = null;
  }

  get currentDatabase() {
    const databaseName = this.currentDatabaseName || 'default';
    return this.databases[databaseName];
  }

  _write(chunk, enc, next) {
    this.next = next;
    this.buffer.add(chunk);

    switch (this.state) {
      case STATES.INSERT:
        this.continueInsert();
        break;
      case STATES.USE:
        this.runUseDatabase();
        break;
      case STATES.CREATE:
        this.runReadSchema();
        break;
      case STATES.SEEKING_EOL:
        this.runSeekEOL();
        break;
      default:
        this.determineState();
        break;
    }
  }

  determineState() {
    const indexOfCreate = this.buffer.indexOf(CREATE_TABLE_BUFFER, undefined, false);
    const indexOfInsert = this.buffer.indexOf(INSERT_INTO_BUFFER, undefined, false);
    const indexOfUse = this.buffer.indexOf(USE_BUFFER, undefined, false);

    const positions = [
      indexOfCreate,
      indexOfInsert,
      indexOfUse,
    ].filter((val) => val !== -1);

    const nearest = Math.min(...positions);

    switch (nearest) {
      case indexOfCreate:
        this.state = STATES.CREATE;
        this.runReadSchema();
        break;
      case indexOfInsert:
        this.state = STATES.INSERT;
        this.runInsert();
        break;
      case indexOfUse:
        this.state = STATES.USE;
        this.runUseDatabase();
        break;
      default:
        this.sqlWriter.writeTailingText(this.buffer.buffer.toString());
        this.state = null;
        this.next();
        break;
    }
  }

  switchCurrentTable(tableName) {
    if (this.currentDatabase.tables[tableName]) {
      this.sqlWriter.setSchema(this.currentDatabase.tables[tableName]);
      if (this.allText !== '') this.sqlWriter.setPreText(this.allText);
      this.allText = '';
    } else {
      throw new Error(`The schema for table '${tableName}' has not been set yet. Your dump needs a CREATE TABLE query.`);
    }
  }

  runInsert() {
    const contents = this
      .buffer
      .getContentBetweenNext(INSERT_INTO_BUFFER, VALUES_BUFFER, {
        moveToEnd: true,
      });

    if (contents === undefined) {
      this.next();
    } else {
      this.switchCurrentTable(cleanName(contents));
      this.continueInsert();
    }
  }

  continueInsert() {
    const queueItem = this.buffer.getNextCommandParenSet();

    if (!queueItem && this.buffer.isAtEnd) {
      return this.next();
    }

    if (queueItem === COMMAND_EXEC_BUFFER) {
      this.buffer.clean();
      this.state = null;
      this.sqlWriter.writeFromBuffers();

      const valuesPosition = this.buffer.indexOf(VALUES_BUFFER);
      const createPosition = this.buffer.indexOf(CREATE_TABLE_BUFFER);
      if (valuesPosition !== -1 && (createPosition === -1 || valuesPosition < createPosition)) {
        this.sqlWriter.setPreText(this.buffer.slice(0, valuesPosition + VALUES_BUFFER.length).toString());
      }

      return this.determineState();
    }

    this.sqlWriter.setValues(queueItem);
    return this.continueInsert();
  }

  switchCurrentDatabase(databaseName) {
    if (!this.databases[databaseName]) {
      this.databases[databaseName] = { tables: {} };
    }

    this.currentDatabaseName = databaseName;
  }

  runUseDatabase() {
    const textPosition = this.buffer.indexOf(COMMAND_EXEC_BUFFER);
    if (textPosition !== -1) {
      this.allText += this.buffer.slice(0, textPosition + COMMAND_EXEC_BUFFER.length).toString();
    }

    const contents = this
      .buffer
      .getContentBetweenNext(USE_BUFFER, COMMAND_EXEC_BUFFER, {
        moveToEnd: true,
      });

    if (contents === undefined) {
      this.next();
    } else {
      this.switchCurrentDatabase(cleanName(contents));
      this.runSeekEOL();
    }
  }

  runSeekEOL() {
    const end = this.buffer.skipToEndOfCommand();

    this.buffer.clean();
    this.state = null;

    if (end === COMMAND_EXEC_BUFFER) {
      this.determineState();
    } else if (!end) {
      this.state = STATES.SEEKING_EOL;
      this.next();
    }
  }

  runReadSchema() {
    const textPosition = this.buffer.indexOf(VALUES_BUFFER);
    if (textPosition !== -1) {
      this.allText += this.buffer.slice(0, textPosition + VALUES_BUFFER.length).toString();
    }

    const tableString = this
      .buffer
      .getContentBetweenNext(CREATE_TABLE_BUFFER, BRACE_OPEN_BUFFER, {
        moveToEnd: true,
      });

    if (tableString === undefined) {
      this.next();
    } else {
      const tableName = cleanName(tableString);
      const contents = this.buffer.getNextCommandParenSet();

      if (contents) {
        const createContents = contents.toString().trim();
        const columns = buildSchema(createContents);
        this.currentDatabase.tables[tableName] = {
          databaseName: this.currentDatabaseName,
          tableName,
          columns,
        };

        this.runSeekEOL();
      } else {
        this.next();
      }
    }
  }

}

module.exports = SQLStreamer;
