const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp');

class Tools {
    static buildSchema(commandContents) {
        const commands = commandContents.split('\n');
        const schema = [];
        const ignoreList = [
            'INDEX',
            'KEY',
            'FULLTEXT',
            'SPATIAL',
            'CONSTRAINT',
            'PRIMARY',
            'UNIQUE',
            'FOREIGN',
        ];

        commands.forEach((command) => {
            const cleanCommand = command.slice(0, -1).trim();

            if (cleanCommand && !Tools.startsWithAny(cleanCommand, ignoreList)) {
                const [name, type] = cleanCommand.split(' ');
                const column = { name: Tools.cleanName(name), type };
                schema.push(column);
            }
        });

        return schema;
    }

    static cleanName(str) {
        return str.replace(/['"`]+/g, '').trim();
    }

    static createFileWriter({ directory, filename, append = false }) {
        const formattedDir = path.format({ dir: directory });
        const normalizedDir = path.normalize(formattedDir);
        const filepath = `${normalizedDir}${filename}`;

        mkdirp.sync(normalizedDir);

        if (append !== true) {
            fs.writeFileSync(filepath, '');
        }

        return fs.createWriteStream(filepath, {flags:'a'});
    }

    static startsWithAny(str, stringArray) {
        for (let i = 0; i < stringArray.length; i += 1) {
            if (str.startsWith(stringArray[i])) {
                return true;
            }
        }
        return false;
    }

}

module.exports = Tools;